# The name of the package
# Just says that you have a subdirectory
ATLAS_SUBDIR(JetSelectionHelper)

# Add binary
# this adds a library and tells it where to look for the ATLAS libraries
# like xAODEventInfo
ATLAS_ADD_LIBRARY ( JetSelectionHelperLib JetSelectionHelper/JetSelectionHelper.h src/JetSelectionHelper.cxx
		  PUBLIC_HEADERS JetSelectionHelper
		  LINK_LIBRARIES xAODEventInfo
				 xAODRootAccess
				 xAODJet)

